package main

import (
	"os"
)

type Config struct {
	serviceName string
	dotKeys     string
	idRsa       string
	idRsaPub    string
	dotLogs     string
}

type FcmConfig struct {
	api       string
	serverKey string
}

var config = Config{
	serviceName: getEnv("SERVICE_NAME", "lekana.notifier"),
	dotKeys:     getEnv("DOT_KEYS", ".keys"),
	idRsa:       getEnv("ID_RSA", ".keys/id_rsa"),
	idRsaPub:    getEnv("ID_RSA_PUB", ".keys/id_rsa.pub"),
	dotLogs:     getEnv("DOT_LOGS", ".logs"),
}

var fcmConfig = FcmConfig{
	api:       getEnv("FCM_API", "https://fcm.googleapis.com/fcm/send"),
	serverKey: getEnv("FCM_SERVER_KEY", ""),
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}

	return fallback
}
