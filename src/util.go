package main

import (
	"strconv"
	"strings"
	"time"
)

func timestampMills() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}

func timestampUnix() int32 {
	return int32(time.Now().Unix())
}

func uid() string {
	t := time.Now().UnixNano() / int64(time.Millisecond)
	return config.serviceName + strconv.FormatInt(t, 10)
}

func formatToSign(msg string) string {
	replacer := strings.NewReplacer(";", "", "\n", "", "\r", "", " ", "")
	return strings.TrimSpace(replacer.Replace(msg))
}
